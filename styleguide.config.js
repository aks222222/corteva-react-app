const path = require("path");

const overrides = require("react-app-rewired/config-overrides");
const webpackConfig = require("react-scripts/config/webpack.config.js");

module.exports = {
    webpackConfig(webpackEnv) {
        const config = overrides.webpack(webpackConfig(webpackEnv), webpackEnv);

        return config;
    },
    // skipComponentsWithoutExample: true,
};
