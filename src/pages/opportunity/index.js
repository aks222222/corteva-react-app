import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import "./index.scss";
import Header from "components/common/header";
import Footer from "components/common/footer";
import resData from "./resData";
import Spinner from "components/common/spinner";
import TextField from '@mui/material/TextField';
import Autocomplete from '@mui/material/Autocomplete';
import _ from 'lodash';
import { Typeahead } from 'react-bootstrap-typeahead'; 

const CortevaOpportunity = (props) => {

  console.log("resData...", resData);
  // let opportunity = resData.opportunity;
  // const Navigate = useNavigate();
  const handleOpportunity = (id) => {
    // Navigate("/opportunity-details");
    console.log("get opportunity id..", id)
  };
  const handleOpportunityDelete = (id) => {
    console.log("get opportunity id..", id)

    var request = {
      type: "delete",
      object: "Opportunity",
      data: {"Id" : id},
    };
    window.bridge.sendRequest(request, function (responseData) {

      console.log("response delete..", JSON.stringify(responseData.data));
      setIsOpportunityDeleted(true);
    });


  };
  const handleOpportunityEdit = (item) => {
    console.log("get opportunity id..", item)
    setOpportunityEdited(item);

  };
  const handleOpportunityEditCancel = (id) => {
    console.log("get opportunity id..", id)
    setOpportunityEdited({});

  };

  const handleOpportunityChange = (e) => {
    let value = e.target.value;
    let name = e.target.name
    opportunityEdit.name = value
    setOpportunityEdited(opportunityEdit)
  };
  const handleOpportunitySave = () => {

    var request = {
      type: "update",
      object: "Opportunity",
      data: opportunityEdit,
    };
    window.bridge.sendRequest(request, function (responseData) {

      console.log("response saved..", JSON.stringify(responseData.data));
      setOpportunityEdited({});
    });


  };

  const [opportunity, setOpportunity] = useState([]);
  const [filterOpportunity, setFilterOpportunity] = useState([]);
  const [isLoaded, setIsLoaded] = useState(false);
  const [opportunityMeta, setOpportunityMeta] = useState([]);
  const [metavalue, setMetavalue] = React.useState('00B1N000007kZAyUAM');
  const [isOpportunityDeleted, setIsOpportunityDeleted] = useState(false);
  const [opportunityEdit, setOpportunityEdited] = useState( {});

  useEffect(() => {
    var request = {
      type: "select",
      object: "Opportunity",
      data: {"query":"Select Id, Name, AccountId, Type, Season__c, StageName, CreatedDate, SignatureDate__c  from Opportunity"},
    };
    window.bridge.sendRequest(request, function (responseData) {
      // alert('Javascript got its read response: ' + JSON.stringify(responseData));
      console.log("response..", JSON.stringify(responseData.data));
      setOpportunity(responseData.data);
      setFilterOpportunity(responseData.data);
      setIsLoaded(true);
    });

    console.log("get opportunity...list..", opportunity);
  }, []);

  useEffect(() => {
    var request = {
      type: "listviewInfo",
      object: "Opportunity",
      data: {},
    };
    window.bridge.sendRequest(request, function (responseData) {
      console.log("listviewInfo Opportunity response..",JSON.stringify(responseData.data));
      var obj = responseData.data;
      var metaData =  [];

      _.forOwn(obj,function(item, key) {
        metaData.push({ id : key, value : item});
        //metaData.push(item);
      });
      //var jsonArray =  JSON.parse(JSON.stringify(metaData))
      setOpportunityMeta(metaData);
      //console.log('metaData--',JSON.stringify(obj));
    });

  }, []);

  // useEffect(() => {
  //   console.log('request metavalue --',metavalue);
  //   var request = {
  //     type: "listviewmetadata",
  //     object: "Opportunity",
  //     data: {"@@listviewid" : metavalue },
  //   };
  //   console.log('request--',JSON.stringify(request));
  //   window.bridge.sendRequest(request, function (responseData) {
  //     console.log("filtered listviewmetadata  response..", JSON.stringify(responseData.data));
  //   });

  // }, []);

  const handleSearch = (e) => {
    const filterOpportunityData = e.target.value
      ? filterOpportunity.filter((rec) =>
          rec.Name.toLowerCase().includes(e.target.value.toLowerCase().trim())
        )
      : opportunity;
    setFilterOpportunity(filterOpportunityData);
  };
  const handleMeta = (e) => {
    const metaselectedData = e.target.value
    setMetavalue(metaselectedData);
    console.log("setMetavalue state..", metaselectedData);
    // var request = {
    //   type: "listviewmetadata",
    //   object: "Opportunity",
    //   data: {"@@listviewid" : metaselectedData },
    // };
    var request = {
      type: "select",
      object: "Opportunity",
      data: {"query":`Select Id, Name, AccountId, Type, Season__c, StageName, CreatedDate, SignatureDate__c  from Opportunity Where id = '${metaselectedData}' `},
    };
    console.log('request--',JSON.stringify(request));
    window.bridge.sendRequest(request, function (responseData) {
      console.log("filtered listviewmetadata  response..", JSON.stringify(responseData.data));
    });

  };
  console.log("opportunityMeta state..", JSON.stringify(opportunityMeta));
  const metaOptions = [
    { label: 'My Opportunities', id: 1 },
    { label: 'Recenty Viewd', id: 2 },
    { label: 'Closing this Month', id: 3 },
    { label: 'My Prepared CTB', id: 4 }
  ];
    

  let metaList = opportunityMeta.length > 0
    	&& opportunityMeta.map((item, i) => {
      return (
        <option key={item.id} value={item.id} >{item.value}</option>
      )
    }, this);


  return (
    <div className="opportunity-wrapper">
      <Header handleSearch={handleSearch} />
      {/* <Header/> */}
      
      <div className="opportunity-content">
        <h2 className="opportunity-content-title">
          My Prepared CTB
          <select onChange={handleMeta}>
            {metaList}
          </select>
          <span className="opportunity-content--record">
            ({filterOpportunity.length})
          </span>
        </h2>

        {isLoaded ? (
          <div className="opportunity-content-table">
            <table>
              <tr>
                <th>Opportunity Name</th>
                <th>Account Name</th>
                <th>Type</th>
                <th>Season</th>
                <th>Stage</th>
                <th>Creation Date</th>
                <th>Signature Date</th>
                <th>Action</th>
              </tr>
              {filterOpportunity.map((item, index) => {
                return (
                  opportunityEdit &&  opportunityEdit.Id== item.Id ? 
                  <tr key={index}>
                    <td> <input type="text" name="Name" value={opportunityEdit.Name ? opportunityEdit.Name : ""} onChange={handleOpportunityChange} /></td>
                    <td> <input type="text" name="AccountId" value={opportunityEdit.AccountId ? opportunityEdit.AccountId : ""} /></td>
                    <td> <input type="text" name="Type" value={opportunityEdit.Type ? opportunityEdit.Type : ""} /></td>
                    <td> <input type="text" name="Season__c" value={opportunityEdit.Season__c ? opportunityEdit.Season__c : ""} /></td>
                    <td> <input type="text" name="StageName" value={opportunityEdit.StageName ? opportunityEdit.StageName : ""} /></td>
                    <td> <input type="date" name="CreatedDate" value={opportunityEdit.CreatedDate ? opportunityEdit.CreatedDate : ""} /></td>
                    <td> <input type="date" name="SignatureDate__c" value={opportunityEdit.SignatureDate__c ? opportunityEdit.SignatureDate__c : ""} /></td>
                    <td ><button onClick={handleOpportunityEditCancel}> Cancel</button> | <button  onClick={()=>handleOpportunitySave()}> Save</button> </td>
                  </tr>
                  :
                  <tr key={index}>
                    <td  onClick={()=>handleOpportunity(item.Id)}>{item.Name ? item.Name : "-"}</td>
                    <td  onClick={()=>handleOpportunity(item.Id)}>{item.AccountId ? item.AccountId : "-"}</td>
                    <td  onClick={()=>handleOpportunity(item.Id)}>{item.Type ? item.Type : "-"}</td>
                    <td  onClick={()=>handleOpportunity(item.Id)}>{item.Season__c ? item.Season__c : "-"}</td>
                    <td  onClick={()=>handleOpportunity(item.Id)}>{item.StageName ? item.StageName : "-"}</td>
                    <td  onClick={()=>handleOpportunity(item.Id)}>{item.CreatedDate ? item.CreatedDate : "-"}</td>
                    <td  onClick={()=>handleOpportunity(item.Id)}>{item.SignatureDate__c ? item.SignatureDate__c : "-"}</td>
                    <td ><button  onClick={()=>handleOpportunityEdit(item)}> Edit</button> | <button  onClick={()=>handleOpportunityDelete(item.Id)}> Delete</button> </td>
                  </tr>
                );
              })}
            </table>
          </div>
        ) : (
          <Spinner />
        )}
      </div>
      <Footer />
    </div>
  );
};

export default CortevaOpportunity;
