import React from "react";
import BreadCrumbs from "../breadcrumbs";
import { useNavigate } from "react-router-dom";

const FarmFieldEdition = () => {
  const Navigate = useNavigate();
  const handleFarmFields = () => {
    Navigate("/farm-fields");
  };
  return (
    <div className="opportunity-wrapper">
      <header className="opportunity-header-wrapper">
        <div className="opportunity-header-wrapper-content">
          <div>
            <h2 className="opportunity-header-wrapper-content--text">
              <span onClick={handleFarmFields} className="navigate">
                &#60;
              </span>
              Opportunities A
            </h2>
            <p>Farm CDG -2022- Commitment</p>
          </div>
        </div>
      </header>
      <div className="opportunity-content">
        <div className="opportunity-content-heading">
          <h2 className="opportunity-content-title">Farm Field Edition</h2>
          <div>
            <button className="custom-button-transparent">Cancel</button>
            <button className="custom-button-blue">Save</button>
          </div>
        </div>

        <div className="section-details">
          {/* <div className="section-details-title">
            <h2 className="section-details-title--text">CTB Information</h2>
          </div> */}
          <div className="section-details-content">
            <ul className="section-details-content-form">
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Field Name</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Water Index</label>
                    <select>
                      <option>0</option>
                      <option>1</option>
                      <option>2</option>
                    </select>
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Surface Area (HA)</label>
                    <input type="text" />
                  </div>
                </div>
              </li>
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Soil Type</label>
                    <select>
                      <option>0</option>
                      <option>1</option>
                      <option>2</option>
                    </select>
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Legal Entity Name</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Roots Depth (CM)</label>
                    <select>
                      <option>0</option>
                      <option>1</option>
                      <option>2</option>
                    </select>
                  </div>
                </div>
              </li>
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>City</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Drainage</label>
                    <select>
                      <option>0</option>
                      <option>1</option>
                      <option>2</option>
                    </select>
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Irrigation</label>
                    <select>
                      <option>0</option>
                      <option>1</option>
                      <option>2</option>
                    </select>
                  </div>
                </div>
              </li>

              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Latitude</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Longitude</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FarmFieldEdition;
