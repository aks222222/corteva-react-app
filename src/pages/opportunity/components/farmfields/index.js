import React from "react";
import resData from "../../resData";
import BreadCrumbs from "../breadcrumbs";
import { imagePath } from "utils/assetsHelper";
import { useNavigate } from "react-router-dom";
import "./index.scss";

const FarmFields = () => {
  const farmfields = resData.farmfields;
  const Navigate = useNavigate();
  const handleFarmFieldEdition = () => {
    Navigate("/farm-fields-edition");
  };
  return (
    <div className="opportunity-wrapper">
      <div className="farmfields-wrapper">
        <BreadCrumbs />
        <div className="opportunity-content">
          <h2 className="opportunity-content-title">Farm Fields</h2>
          <div className="opportunity-content-table">
            <table>
              <tr>
                <th>Field Name</th>
                <th>Surface (Hertare)</th>
                <th>Water Index</th>
                <th>Soil Type</th>
                <th>Irrigation</th>
                <th>
                  <button className="custom-button-white">+ Farm Field</button>
                </th>
              </tr>
              {farmfields.map((item, index) => {
                return (
                  <tr onClick={handleFarmFieldEdition} key={index}>
                    <td>{item.fieldname}</td>
                    <td>{item.surface}</td>
                    <td>{item.waterindex}</td>
                    <td>{item.soiltype}</td>
                    <td>{item.irrigation}</td>
                    <td align="center">
                      <span>
                      <img src={imagePath('/home/edit.png')} alt="edit" />
                      </span>
                    </td>
                  </tr>
                );
              })}
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FarmFields;
