import React from "react";
import "./index.scss";

const Header = () => {
  return (
    <header className="opportunity-header-wrapper">
      <div className="opportunity-header-wrapper-content">
        <div>
          <h2 className="opportunity-header-wrapper-content--text">
            Opportunities A
          </h2>
          <p>Farm CDG -2022- Commitment</p>
        </div>
        <button
          type="button"
          className="opportunity-header-wrapper-content--button"
        >
          Sign & Print CTB
        </button>
      </div>
    </header>
  );
};

export default Header;
