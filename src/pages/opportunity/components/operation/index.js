import React from "react";
import resData from "../../resData";
import BreadCrumbs from "../breadcrumbs";
import { imagePath } from "utils/assetsHelper";
import { useNavigate } from "react-router-dom";
import "./index.scss";

const OperationList = () => {
  const operationlist = resData.operationlist;
  // const Navigate = useNavigate();
  const handleOperationEdition = () => {
    // Navigate("/operation-list-edition");
  };
  return (
    <div className="opportunity-wrapper">
      <div className="farmfields-wrapper">
        <BreadCrumbs />
        <div className="opportunity-content">
          <h2 className="opportunity-content-title">Operation List</h2>
          <div className="opportunity-content-table">
            <table>
              <tr>
                <th>Name</th>
                <th>Account</th>
                <th>Equipment Type</th>
                <th>Number</th>
                <th>Source</th>
                <th>Date Of Observation</th>
                <th>
                  <button className="custom-button-white">+ Operation</button>
                </th>
              </tr>
              {operationlist.map((item, index) => {
                return (
                  <tr onClick={handleOperationEdition} key={index}>
                    <td>{item.name}</td>
                    <td>{item.account}</td>
                    <td>{item.equipmenttype}</td>
                    <td>{item.number}</td>
                    <td>{item.source}</td>
                    <td>{item.doobservation}</td>
                    <td align="center">
                      <span>
                        <img src={imagePath('/home/edit.png')} alt="edit" />
                      </span>
                      <span>
                        <img src={imagePath('/home/delete.png')} alt="delete" />
                      </span>
                    </td>
                  </tr>
                );
              })}
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OperationList;
