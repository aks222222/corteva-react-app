import React from "react";
import BreadCrumbs from "../breadcrumbs";
import { useNavigate } from "react-router-dom";

const OperationListEdition = () => {
  const Navigate = useNavigate();
  const handleFarmFields = () => {
    Navigate("/opportunity-contact");
  };
  return (
    <div className="opportunity-wrapper">
      <header className="opportunity-header-wrapper">
        <div className="opportunity-header-wrapper-content">
          <div>
            <h2 className="opportunity-header-wrapper-content--text">
              <span onClick={handleFarmFields} className="navigate">
                &#60;
              </span>
              Opportunities A
            </h2>
            <p>Farm CDG -2022- Commitment</p>
          </div>
        </div>
      </header>
      <div className="opportunity-content">
        <div className="opportunity-content-heading">
          <h2 className="opportunity-content-title">Operation Details</h2>
          <div>
            <button className="custom-button-transparent">Cancel</button>
            <button className="custom-button-blue">Save</button>
          </div>
        </div>

        <div className="section-details">
          <div className="section-details-title">
            <h2 className="section-details-title--text">Details</h2>
          </div>
          <div className="section-details-content">
            <ul className="section-details-content-form">
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Name</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Account</label>
                    <input type="text" />
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>

        <div className="section-details">
          <div className="section-details-title">
            <h2 className="section-details-title--text">Equipment</h2>
          </div>
          <div className="section-details-content">
            <ul className="section-details-content-form">
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Grain Silos</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Variable rate seeding equipment</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Planters</label>
                    <input type="text" />
                  </div>
                </div>
              </li>
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Grain Hoppers</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Grain Harvesters</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Grain Dryers</label>
                    <input type="text" />
                  </div>
                </div>
              </li>
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Silage Harvesters</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Source</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Inoculant Applicators</label>
                    <input type="text" />
                  </div>
                </div>
              </li>
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Silage Harvesters</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OperationListEdition;
