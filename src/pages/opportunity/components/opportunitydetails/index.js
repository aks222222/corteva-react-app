import React from "react";
import { imagePath } from "utils/assetsHelper";
import resData from "../../resData";
import "./index.scss";

const OpportunityDetailsTab = () => {
  const opportunitydetails = resData.opportunitydetails;

  return (
    <div className="opportunity-details-wrapper">
      <div className="section-details">
        <div className="section-details-title">
          <h2 className="section-details-title--text">Details</h2>
          <img src={imagePath('/home/edit.png')} alt="edit" />
        </div>
        <div className="section-details-content">
          <ul className="section-details-content-item">
            {opportunitydetails.map((item, index) => {
              return (
                <li className="section-details-content-item-list" key={index}>
                  <div className="section-details-content-item-data">
                    <h3 className="section-details-content-item-data--title">
                      {item.title}
                    </h3>
                    <p className="section-details-content-item-data--desc">
                      {item.description}
                    </p>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      <div className="section-details">
        <div className="section-details-title">
          <h2 className="section-details-title--text">CTB Information</h2>
        </div>
        <div className="section-details-content">
          <ul className="section-details-content-form">
            <li className="section-details-content-form-item">
              <label>Signature Date</label>
              <input type="text" readOnly className="signaturedate" value="19/02/2022"/>
            </li>

            <li className="section-details-content-form-item">
              <label>Agronomic Advice</label>
              <textarea></textarea>
            </li>
            <li className="section-details-content-form-item">
              <div className="section-details-content-form-2col">
                <div className="section-details-content-form-2col-item">
                  <label>Notes</label>
                  <textarea></textarea>
                </div>
                <div className="section-details-content-form-2col-item">
                  <label>Legal Notes</label>
                  <textarea></textarea>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
      <div className="section-details">
        <div className="section-details-title">
          <h2 className="section-details-title--text">Files</h2>
        </div>
        <div className="section-details-content">
          <p>No files found</p>
        </div>
      </div>
    </div>
  );
};

export default OpportunityDetailsTab;
