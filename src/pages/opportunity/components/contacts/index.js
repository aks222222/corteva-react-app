import React from "react";
import resData from "../../resData";
import BreadCrumbs from "../breadcrumbs";
import { imagePath } from "utils/assetsHelper";
import { useNavigate } from "react-router-dom";
import "./index.scss";

const OpportunityContact = () => {
  const opportunitycontact = resData.opportunitycontact;
  const Navigate = useNavigate();
  const handleFarmFieldEdition = () => {
    Navigate("/opportunity-contact-edition");
  };
  return (
    <div className="opportunity-wrapper">
      <div className="farmfields-wrapper">
        <BreadCrumbs />
        <div className="opportunity-content">
          <h2 className="opportunity-content-title">
            Opportunity Contact Role
          </h2>
          <div className="opportunity-content-table">
            <table>
              <tr>
                <th>Role</th>
                <th>Contact</th>
                <th>Mobile</th>
                <th>Opt In</th>
                <th>
                  <button className="custom-button-white">
                    + Contact Role
                  </button>
                </th>
              </tr>
              {opportunitycontact.map((item, index) => {
                return (
                  <tr onClick={handleFarmFieldEdition} key={index}>
                    <td>{item.role}</td>
                    <td>{item.contact}</td>
                    <td>{item.mobile}</td>
                    <td>{item.optin}</td>
                    <td align="center">
                      <span>
                        <img src={imagePath('/home/edit.png')} alt="edit" />
                      </span>
                      <span>
                        <img src={imagePath('/home/delete.png')} alt="delete" />
                      </span>
                    </td>
                  </tr>
                );
              })}
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default OpportunityContact;
