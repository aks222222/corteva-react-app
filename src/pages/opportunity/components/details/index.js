import React, { useState } from "react";
import Header from "../header";
import "./index.scss";
import Footer from "../../../../components/common/footer";
import OpportunityProducts from "../products";
import AccountsAndContacts from "../accountsandcontacts";
import OpportunityActivities from "../opportunityactivities";
import OpportunityDetailsTab from "../opportunitydetails";

const OpportunityDetails = (props) => {
  const [activeTab, setActiveTab] = useState(1);
  const tabs = props.tabs;
  const handleTabs = (id) => {
    console.log("clicked id:..", id);
    setActiveTab(id);
  };
  return (
    <div className="opportunity-wrapper">
      <Header />
      <div className="opportunity-details">
        <div className="opportunity-details-tabs">
          <ul className="opportunity-details-tabs-list">
            {tabs.map((item, index) => {
              return (
                <li
                  className={`opportunity-details-tabs-list-item ${
                    item.id === activeTab ? "active" : ""
                  }`}
                  id={index}
                  onClick={() => handleTabs(item.id)}
                >
                  {item.title}
                </li>
              );
            })}
            {/* <li className="opportunity-details-tabs-list-item active">
              Products
            </li>
            <li className="opportunity-details-tabs-list-item">
              Accounts & Contacts
            </li>
            <li className="opportunity-details-tabs-list-item">
              Opprotunity Details
            </li>
            <li className="opportunity-details-tabs-list-item">
              Opprotunity Activities
            </li> */}
          </ul>
        </div>
        {activeTab === 1 ? <OpportunityProducts /> : ""}
        {activeTab === 2 ? <AccountsAndContacts /> : ""}
        {activeTab === 3 ? <OpportunityDetailsTab /> : ""}
        {activeTab === 4 ? <OpportunityActivities /> : ""}
      </div>
      <Footer />
    </div>
  );
};

OpportunityDetails.defaultProps = {
  tabs: [
    {
      id: 1,
      title: "Products",
    },
    {
      id: 2,
      title: "Accounts & Contacts",
    },
    {
      id: 3,
      title: "Opprotunity Details",
    },
    {
      id: 4,
      title: "Opprotunity Activities",
    },
  ],
};

export default OpportunityDetails;
