import React,{useState, useEffect} from "react";
import { useNavigate } from "react-router-dom";
import resData from "../../resData";
import { imagePath } from "utils/assetsHelper";

const OpportunityProducts = (props) => {
  const ProductsDetail = resData.products;
  const IncentivesDetail = resData.incentives;
  // const Navigate = useNavigate();
  const handleIncentives = () => {
    // Navigate("/incentive-edition");
  };

  const [productList, setProductList]=useState([]);
  const [incentiveList, setincentiveList]=useState([]);

  useEffect(()=>{

    var request = { 
      "type"   : "read",
      "object" : "OpportunityLineItem",
      "data"   : {}
  };
  window.bridge.sendRequest(request, function (responseData) { 
    // alert('Javascript got its read response: ' + JSON.stringify(responseData)); 
    console.log("response..",JSON.stringify(responseData.data))
    setProductList(responseData.data)
  });
    
    console.log("get product...list..",productList)
  },[])

  useEffect(()=>{

    var request = { 
      "type"   : "read",
      "object" : "Incentive__c",
      "data"   : {}
  };
  window.bridge.sendRequest(request, function (responseData) { 
    // alert('Javascript got its read response: ' + JSON.stringify(responseData)); 
    console.log("response..",JSON.stringify(responseData.data))
    setincentiveList(responseData.data)
  });
    
    console.log("get incentive...list..",incentiveList)
  },[])

  return (
    <div className="opportunity-wrapper">
      <div className="opportunity-content">
        <div className="opportunity-content-table">
          <table>
            <tr>
              <th>Status</th>
              <th>Family</th>
              <th>Product</th>
              <th>Surface</th>
              <th>Quantity</th>
              <th>UOM</th>
              <th>Dealer</th>
              <th>Farm Field</th>
              <th align="right">
                <button className="custom-button-white">+ Product</button>
              </th>
            </tr>
            {productList.map((item, index) => {
              return (
                <tr key={index}>
                  <td>
                    <select>
                      <option value={item.SalesCycleProductStatus__c}>{item.SalesCycleProductStatus__c?item.SalesCycleProductStatus__c:"-"}</option>
                      <option value="Committed">Committed</option>
                      <option value="Not Committed">Not Committed</option>
                    </select>
                  </td>
                  <td>{item.CropType__c?item.CropType__c:"-"}</td>
                  <td>{item.Hybrid_Name__c?item.Hybrid_Name__c:"-"}</td>
                  <td>{item.PlantedArea__c?item.PlantedArea__c:"-"}</td>
                  <td>{item.Unit_of_Measure__c?item.Unit_of_Measure__c:"-"}</td>
                  <td>{item.fStandardUOM__c?item.fStandardUOM__c:"-"}</td>
                  <td>{item.Dealer__c?item.Dealer__c:"-"}</td>
                  <td>{item.FarmField__c?item.FarmField__c:"-"}</td>
                  <td align="right">
                    <span>
                      <img src={imagePath('/home/edit.png')} alt="edit" />
                    </span>
                    <span>
                      <img src={imagePath('/home/delete.png')} alt="Delete" />
                    </span>
                    <span>
                      <img src={imagePath('/home/new.png')} alt="Copy" />
                    </span>
                  </td>
                </tr>
              );
            })}
          </table>
        </div>
      </div>

      <div className="opportunity-content">
        <h2 className="opportunity-content-title">Incentives</h2>
        <div className="opportunity-content-table">
          <table>
            <tr>
              <th>Incentive</th>
              <th>Product</th>
              <th>Description</th>
              <th>
                <button
                  className="custom-button-white"
                  onClick={handleIncentives}
                >
                  + Incentives
                </button>
              </th>
            </tr>
            {incentiveList.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.Name?item.Name:"-"}</td>
                  <td>{item.IncentiveType__c?item.IncentiveType__c:"-"}</td>
                  <td>{item.Description__c?item.Description__c:"-"}</td>
                  <td align="right">
                  <span>
                      <img src={imagePath('/home/edit.png')} alt="edit" />
                    </span>
                    <span>
                      <img src={imagePath('/home/delete.png')} alt="Delete" />
                    </span>
                    <span>
                      <img src={imagePath('/home/new.png')} alt="Copy" />
                    </span>
                  </td>
                </tr>
              );
            })}
          </table>
        </div>
      </div>
    </div>
  );
};

export default OpportunityProducts;
