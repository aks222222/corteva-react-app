import React from "react";
import { useNavigate } from "react-router-dom"
import { imagePath } from "utils/assetsHelper";

const ProductEdition = () =>{
     const Navigate = useNavigate();
  const handleFarmFields = () => {
    Navigate("/opportunity-details");
  };
    return(
        <div className="opportunity-wrapper">
      <header className="opportunity-header-wrapper">
        <div className="opportunity-header-wrapper-content">
          <div>
            <h2 className="opportunity-header-wrapper-content--text">
              <span onClick={handleFarmFields} className="navigate">
                &#60;
              </span>
              Opportunities A
            </h2>
            <p>Farm CDG -2022- Commitment</p>
          </div>
        </div>
      </header>
        <div className="opportunity-content">
        <div className="opportunity-content-heading">
          <h2 className="opportunity-content-title">Product Edition</h2>
          <div>
            <button className="custom-button-blue">Save</button>
          </div>
        </div>
        <div className="section-details-dark">
        <ul className="section-details-content-form">
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Product</label>
                    <input type="text" />
                    <img src={imagePath('/home/search.png')} alt="search" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                  <label>Dealer</label>
                    <input type="text" />
                    <img src={imagePath('/home/search.png')} alt="search" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                  <label>Sales Cycle Product Status</label>
                    <select>
                      <option>Commited</option>
                      <option>Commited</option>
                      <option>Commited</option>
                    </select>
                  </div>
                </div>
              </li>
            </ul>
            <div className="section-details-dark-seprator">&nbsp;</div>
        </div>

        <div className="section-details">
          <div className="section-details-title">
            <h2 className="section-details-title--text">Seed</h2>
          </div>
          <div className="section-details-content">
            <ul className="section-details-content-form">
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Crop Type</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>*Crop Subtype</label>
                    <select>
                      <option>Corn Grain</option>
                      <option>1</option>
                      <option>2</option>
                    </select>
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                </div>
              </li>
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>*Surface</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>*Dencity</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>UOM</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>*Quantity</label>
                    <input type="text" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>

        <div className="section-details">
          <div className="section-details-title">
            <h2 className="section-details-title--text">Agronomy</h2>
          </div>
          <div className="section-details-content">
            <ul className="section-details-content-form">
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Farm field</label>
                    <input type="text" />
                    <img src={imagePath('/home/search.png')} alt="search" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Crop Usage</label>
                    <select>
                      <option></option>
                      <option>1</option>
                      <option>2</option>
                    </select>
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                </div>
              </li>
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Agronomy</label>
                    <div className="agronomy-desc">
                        <ul className="agronomy-desc-list">
                            <li className="agronomy-desc-list-item"><label>Farm Name::</label><span>Field 1</span></li>
                            <li className="agronomy-desc-list-item"><label>Farm Name::</label><span>Field 1</span></li>
                            <li className="agronomy-desc-list-item"><label>Farm Name::</label><span>Field 1</span></li>
                            <li className="agronomy-desc-list-item"><label>Farm Name::</label><span>Field 1</span></li>
                            <li className="agronomy-desc-list-item"><label>Farm Name::</label><span>Field 1</span></li>
                            <li className="agronomy-desc-list-item"><label>Farm Name::</label><span>Field 1</span></li>
                        </ul>
                    </div>
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>

        <div className="section-details">
          <div className="section-details-title">
            <h2 className="section-details-title--text">Objective</h2>
          </div>
          <div className="section-details-content">
            <ul className="section-details-content-form">
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Objective</label>
                    <select multiple className="multiple-select">
                      <option>Decrese Inputs</option>
                      <option>Decrese Irrigation</option>
                      <option>Early Harvest</option>
                      <option>Extend Crop Rotation</option>
                      <option>Improve Ration  Efficiency</option>
                    </select>
                  </div>
                  <div className="multi-select-btnwrapper">
                      <button>&gt;</button>
                      <button>&lt;</button>
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Available</label>
                    <select multiple className="multiple-select">
                      <option>Decrese Inputs</option>
                      <option>Decrese Irrigation</option>
                    </select>
                  </div>
                  <div className="section-details-content-form-2col-item">
                   &nbsp;
                  </div>
                </div>
              </li>
              
            </ul>
          </div>
        </div>

        <div className="section-details">
          <div className="section-details-title">
            <h2 className="section-details-title--text">Other</h2>
          </div>
          <div className="section-details-content">
            <ul className="section-details-content-form">
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label> Notes</label>
                    <textarea></textarea>
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>

      </div>
      </div>
    )
}

export default ProductEdition