import React from "react";
import "./index.scss";
import { useNavigate } from "react-router-dom";

const BreadCrumbs = () => {
  const Navigate = useNavigate();
  const handleFarmFields = () => {
    Navigate("/opportunity-details");
  };
  return (
    <header className="opportunity-header-wrapper">
      <div className="opportunity-header-wrapper-content">
        <div>
          <h2 className="opportunity-header-wrapper-content--text">
            <span onClick={handleFarmFields} className="navigate">
              &#60;
            </span>
            Opportunities A
          </h2>
          <p>Farm CDG -2022- Commitment</p>
        </div>
      </div>
    </header>
  );
};

export default BreadCrumbs;
