import React,{useState, useEffect} from "react";
import resData from "../../resData";

const OpportunityActivities = () => {
  const incentivesDetail = resData.incentives;
  const task = resData.incentives;

  const [eventList, setProductList]=useState([]);
  const [taskList, setincentiveList]=useState([]);

  useEffect(()=>{

    var request = { 
      "type"   : "read",
      "object" : "Event",
      "data"   : {}
  };
  window.bridge.sendRequest(request, function (responseData) { 
    // alert('Javascript got its read response: ' + JSON.stringify(responseData)); 
    console.log("event response..",JSON.stringify(responseData.data))
    setProductList(responseData.data)
  });
    
    console.log("get event...list..",eventList)
  },[])

  useEffect(()=>{

    var request = { 
      "type"   : "read",
      "object" : "Task",
      "data"   : {}
  };
  window.bridge.sendRequest(request, function (responseData) { 
    // alert('Javascript got its read response: ' + JSON.stringify(responseData)); 
    console.log("task response..",JSON.stringify(responseData.data))
    setincentiveList(responseData.data)
  });
    
    console.log("get task...list..",taskList)
  },[])


  return (
    <div>
      <div className="opportunity-content">
        <div className="opportunity-content-heading">
          <h2 className="opportunity-content-title">Events</h2>
          <button className="custom-button-white">View All Events</button>
        </div>
        <div className="opportunity-content-table">
          <table>
            <tr>
              <th>Subject</th>
              <th>Start Date & Time</th>
              <th>Related To</th>
              <th>Event Type</th>
              <th>Status</th>
              <th>Task Sub Type</th>
              <th>
                <button className="custom-button-white">+ Event</button>
              </th>
            </tr>
            {eventList.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.Subject?item.Subject:"-"}</td>
                  <td>{item.ActivityDate?item.ActivityDate:"-"}</td>
                  <td>{item.WhatId?item.WhatId:"-"}</td>
                  <td>{item.Type?item.Type:"-"}</td>
                  <td>{item.EndDateTime?item.EndDateTime:"-"}</td>
                  <td>{item.EventSubtype?item.EventSubtype:"-"}</td>
                  <td align="right">&nbsp;</td>
                </tr>
              );
            })}
          </table>
        </div>
      </div>

      <div className="opportunity-content">
        <div className="opportunity-content-heading">
          <h2 className="opportunity-content-title">Task</h2>
          <button className="custom-button-white">View All Task</button>
        </div>
        <div className="opportunity-content-table">
          <table>
            <tr>
              <th>Subject</th>
              <th>Due Date</th>
              <th>Related To</th>
              <th>Name</th>
              <th>Status</th>
              <th>Task Sub Type</th>
              <th>Priority</th>
              <th>
                <button className="custom-button-white">+ Tasks</button>
              </th>
            </tr>
            {taskList.map((item, index) => {
              return (
                <tr key={index}>
                  <td>{item.Subject?item.Subject:"-"}</td>
                  <td>{item.ActivityDate?item.ActivityDate:"-"}</td>
                  <td>{item.WhatId?item.WhatId:"-"}</td>
                  <td>{item.WhoId?item.WhoId:"-"}</td>
                  <td>{item.Status?item.Status:"-"}</td>
                  <td>{item.TaskSubtype?item.TaskSubtype:"-"}</td>
                  <td>{item.Priority?item.Priority:"-"}</td>
                  <td align="right">&nbsp;</td>
                </tr>
              );
            })}
          </table>
        </div>
      </div>
    </div>
  );
};

export default OpportunityActivities;
