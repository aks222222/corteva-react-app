import React from "react";
import "./index.scss";
import AccountNavList from "./AccountNavList";
import resData from "../../resData";
import { imagePath } from "utils/assetsHelper";

const AccountsAndContacts = (props) => {
  const account = resData.account;
  const contact = resData.contact;

  return (
    <div className="opportunity-account-wrapper">
      <AccountNavList />
      <div className="section-details">
        <div className="section-details-title">
          <h2 className="section-details-title--text">Account</h2>
          <img src={imagePath('/home/edit.png')} alt="edit" />
        </div>
        <div className="section-details-content">
          <ul className="section-details-content-item">
            {account.map((item,index) => {
              return (
                <li className="section-details-content-item-list" key={index}>
                  <div className="section-details-content-item-data">
                    <h3 className="section-details-content-item-data--title">
                      {item.title}
                    </h3>
                    <p className="section-details-content-item-data--desc">
                      {item.description}
                    </p>
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
      </div>

      <div className="opportunity-content-table">
        <table>
          <tr>
            <th>Name</th>
            <th>Address</th>
            <th>Opt-inCorteva</th>
            <th>Primary Role</th>
            <th>Mobile</th>
            <th>Email</th>
            <th align="right">
              <button className="custom-button-white">+ Contact</button>
            </th>
          </tr>
          {contact.map((item,index) => {
            return (
              <tr key={index}>
                <td>{item.name}</td>
                <td>{item.address}</td>
                <td>{item.optincorteva}</td>
                <td>{item.primaryrole}</td>
                <td>{item.mobile}</td>
                <td>{item.email}</td>
                <td align="right">
                  <span>
                  <img src={imagePath('/home/edit.png')} alt="edit" />
                  </span>
                </td>
              </tr>
            );
          })}
        </table>
      </div>
    </div>
  );
};

export default AccountsAndContacts;
