/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { useNavigate } from "react-router-dom";
import { imagePath } from "utils/assetsHelper";

const AccountNavList = () => {
  // const Navigate = useNavigate();
  const handleFarmFields = () => {
    // Navigate("/farm-fields");
  };
  const handleOpportunityContact = () => {
    // Navigate("/opportunity-contact");
  };
  const handleOperation = () => {
    // Navigate("/operation-list");
  };
  return (
    <div className="opportunity-account-wrapper-nav">
      <ul className="opportunity-account-wrapper-nav-list">
        <li className="opportunity-account-wrapper-nav-list--item">
          <img src={imagePath('/home/edit.png')} />
          Crop Potentials
        </li>
        <li
          className="opportunity-account-wrapper-nav-list--item"
          onClick={handleOperation}
        >
          <img src={imagePath('/home/edit.png')} />
          Operation Details
        </li>
        <li
          className="opportunity-account-wrapper-nav-list--item"
          onClick={handleFarmFields}
        >
          <img src={imagePath('/home/edit.png')} />
          Farm Fields
        </li>
        <li
          className="opportunity-account-wrapper-nav-list--item"
          onClick={handleOpportunityContact}
        >
          <img src={imagePath('/home/edit.png')} />
          Contact Roles
        </li>
      </ul>
    </div>
  );
};

export default AccountNavList;
