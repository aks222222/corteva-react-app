import React from "react";
import BreadCrumbs from "../breadcrumbs";
import { imagePath } from "utils/assetsHelper";
import { useNavigate } from "react-router-dom";

const IncentiveAssign = () => {
  const Navigate = useNavigate();
  const handleIncentive = () => {
    Navigate("/opportunity-details");
  };
  return (
    <div className="opportunity-wrapper">
      <header className="opportunity-header-wrapper">
        <div className="opportunity-header-wrapper-content">
          <div>
            <h2 className="opportunity-header-wrapper-content--text">
              <span onClick={handleIncentive} className="navigate">
                &#60;
              </span>
              Opportunities A
            </h2>
            <p>Farm CDG -2022- Commitment</p>
          </div>
        </div>
      </header>
      <div className="opportunity-content">
        <div className="opportunity-content-heading">
          <h2 className="opportunity-content-title">Incentive assignation</h2>
          <div>
            <button className="custom-button-transparent">Cancel</button>
            <button className="custom-button-blue">Save</button>
          </div>
        </div>

        <div className="section-details">
          {/* <div className="section-details-title">
            <h2 className="section-details-title--text">CTB Information</h2>
          </div> */}
          <div className="section-details-content">
            <ul className="section-details-content-form">
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Opportunity</label>
                    <input
                      type="text"
                      placeholder="2022 - Spain - Opportunity A"
                    />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    <label>Opportunity Product</label>
                    <input type="text" placeholder="Search" />
                    <img src={imagePath('/home/search.png')} alt="search" />
                  </div>
                </div>
              </li>
              <li className="section-details-content-form-item">
                <div className="section-details-content-form-2col">
                  <div className="section-details-content-form-2col-item">
                    <label>Related Incentive</label>
                    <input type="text" placeholder="Search" />
                    <img src={imagePath('/home/search.png')} alt="search" />
                  </div>
                  <div className="section-details-content-form-2col-item">
                    &nbsp;
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default IncentiveAssign;
