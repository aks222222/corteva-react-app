import React, { useState } from "react";
import { Routes, Route, Link, Redirect, useNavigate } from "react-router-dom";
import { imagePath } from "utils/assetsHelper";
import "./index.scss";

const Home = (props) => {
  const applist = props.applist;

  // const navigate = useNavigate();
  const handleOpportunity = () => {
    // navigate("/opportunity");
  };

  return (
    <div className="Landingpage">
      <img src={imagePath('/home/giphy.gif')} className="Landingpage-animate" />

      <h1 className="Landingpage-title" onClick={handleOpportunity}>
        The Heart of the Farm <br />
        "Land and Legacy"
      </h1>
      <p className="cortevalogo-white">
        <img  src={imagePath('/home/corteva-logo-white.webp')} />
      </p>
    </div>
  );
};

export default Home;
