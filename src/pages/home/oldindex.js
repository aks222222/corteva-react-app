import React, { useState } from "react";
import { Routes, Route, Link, Redirect, useNavigate } from "react-router-dom";
import { imagePath } from "utils/assetsHelper";
import Search from "../../components/search";
import cortevaimg from "../../assets/images/home/corteva.jpg";
import img1 from "../../assets/images/home/img1.png";
import img2 from "../../assets/images/home/img2.png";
import img3 from "../../assets/images/home/img3.png";
import img4 from "../../assets/images/home/img4.png";
import img5 from "../../assets/images/home/img5.png";
import img6 from "../../assets/images/home/img6.png";
import "./index.scss";

const Home = (props) => {
  const applist = props.applist;

  const navigate = useNavigate();
  const handleOpportunity = () => {
    navigate("/opportunity");
  };
  const createAccount = () => {
    var request = {
      type: "showCreate",
      object: "Account",
      data: { Name: "John Doe Industries", Phone: "867-5309" },
    };
    window.bridge.sendRequest(request, function (responseData) {
      if (
        responseData.type === "showcreateResponse" &&
        responseData.data.createResult === "TRUE"
      ) {
        alert("Create Success! ObjectId: " + responseData.data.createId);
        //accountID = responseData.data.createId
      } else {
        alert("Error during create: " + responseData.data);
      }
    });
  };

  console.log(applist);
  return (
    <>
      <section className="section-home">
        <h1 style={{ textAlign: "center" }}> React Application </h1>
        <Search />
        <ul className="section-home-applist">
          {applist.map((item, index) => {
            return (
              <li className="section-home-applist-item" key={index}>
                <Link to={`${item.route}`} onClick={handleOpportunity}>
                  <div className="section-home-applist-item-content">
                    <img
                      src={`${item.img}`}
                      width="100px"
                      height="100px"
                      alt={item.label}
                    />
                    <span>{item.label}</span>
                  </div>
                </Link>
              </li>
            );
          })}
        </ul>
        <div className="corteva_logo">
          <img src={cortevaimg} alt="corteva" />
        </div>
      </section>
    </>
  );
};

Home.defaultProps = {
  applist: [
    {
      img: img1,
      label: "Welcome back, Peter Smith!",
      route: "/home",
    },
    {
      img: img2,
      label: "Contacts",
      route: "/contacts",
    },
    {
      img: img3,
      label: "Accounts",
      route: "/accounts",
    },
    {
      img: img4,
      label: "Parametrs",
      route: "/parametrs",
    },
    {
      img: img5,
      label: "CTB",
      route: "/opportunity",
    },
    {
      img: img6,
      label: "Tasks",
      route: "/tasks",
    },
  ],
};

export default Home;
