import React,{Suspense } from "react";
import { Routes, Route } from "react-router-dom";

import Home from "./pages/home";
import CortevaOpportunity from "./pages/opportunity";
import OpportunityDetails from "./pages/opportunity/components/details";
import CortevaAccounts from "./pages/accounts";
import CortevaContacts from "./pages/contacts";
import FarmFields from "./pages/opportunity/components/farmfields";
import FarmFieldEdition from "./pages/opportunity/components/farmfields/FarmFieldEdition";

import OpportunityContact from "./pages/opportunity/components/contacts";
import OpportunityContactEdition from "./pages/opportunity/components/contacts/OpportunityContactEdition";

import OperationList from "./pages/opportunity/components/operation";
import OperationListEdition from "./pages/opportunity/components/operation/OperationListEdition";

import IncentiveAssign from "./pages/opportunity/components/incentives/IncentiveAssign";

import History from 'components/common/history';

import "./index.scss";

function App() {
  return (
    <div className="cbt-wrapper">
    {/* <Home /> */}
      <CortevaOpportunity />
      <OpportunityDetails />
    </div>
      // <div className="cbt-wrapper">
      //   <Routes>
      //     {/* <Route path="/" element={<Home />} /> */}
      //     <Route path="/" element={<CortevaOpportunity />} />
      //     <Route path="/home" element={<CortevaOpportunity />} />
      //     <Route path="/opportunity" element={<CortevaOpportunity />} />
      //     <Route path="/opportunity-details" element={<OpportunityDetails />} />

      //     <Route path="/accounts" element={<CortevaAccounts />} />
      //     <Route path="/contacts" element={<CortevaContacts />} />

      //     <Route path="/farm-fields" element={<FarmFields />} />
      //     <Route path="/farm-fields-edition" element={<FarmFieldEdition />} />

      //     <Route path="/opportunity-contact" element={<OpportunityContact />} />
      //     <Route
      //       path="/opportunity-contact-edition"
      //       element={<OpportunityContactEdition />}
      //     />

      //     <Route path="/operation-list" element={<OperationList />} />
      //     <Route
      //       path="/operation-list-edition"
      //       element={<OperationListEdition />}
      //     />

      //     <Route path="/incentive-edition" element={<IncentiveAssign />} />
      //   </Routes>
      // </div>
  );
}

export default App;
