import React from "react";
import { useNavigate } from "react-router-dom";
import "./index.scss";
import { imagePath } from "utils/assetsHelper";

const Footer = () => {
  // const Navigate = useNavigate();
  const handleOpportunity = () => {
    // Navigate("/opportunity");
    // Navigate("/");
  };
  return (
    <footer className="footer-wrapper">
      <div className="footer-wrapper-content">
        <div className="footer-wrapper-content--homeicon">
          <img src={imagePath('/home/homeico.png')} alt="home" onClick={handleOpportunity} />
        </div>
        <div className="footer-wrapper-content--slogo">
          <img src={imagePath('/home/corteva-logo-white.webp')} alt="corteva" />
        </div>
      </div>
    </footer>
  );
};

export default Footer;
