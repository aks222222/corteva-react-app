import React from "react";
import "./index.scss";
import { imagePath } from "utils/assetsHelper";

const Header = (props) => {
  const handleSearch = props.handleSearch;

  return (
    <header className="header-wrapper">
      <div className="header-wrapper-content">
        <h2 className="header-wrapper-content--text">Opportunities</h2>
        <div className="header-search-wrapper">
          <input
            type="text"
            id="txtSearch"
            name="txtSearch"
            placeholder="search"
            className="header-search-wrapper--input"
            onChange={ handleSearch }
          />
          <img src={imagePath('/home/search.png')} alt="search" />
        </div>
        <button type="button" className="header-wrapper-content--button">
          + Add Opportunity
        </button>
      </div>
    </header>
  );
};

export default Header;
