import React from 'react';
import "./index.scss";

import { imagePath } from 'utils/assetsHelper';

const Spinner = () => {
    return(
        <div className='spinner'>
            <img src={imagePath("/home/spinner-icon.gif")} />
        </div>
    )
}

export default Spinner