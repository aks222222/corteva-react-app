import React from "react";
import { imagePath } from "utils/assetsHelper";
import "./index.scss";
const Search = () => {
  return (
    <div className="search-wrapper">
      <input type="text" name="search" id="search" />
      <label>Search</label>
      <img src={imagePath('/home/search.png')} alt="search" />
    </div>
  );
};

export default Search;
